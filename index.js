// console.log("hello");

let students = [
	"John",
	"Jane",
	"Jeff",
	"Kevin",
	"David"
];

// console.log(students);

// class Dog {
// 	constructor(name,breed,age){
// 		this.name = name;
// 		this.breed = breed;
// 		this.age = age;
// 	}
// }

// let dog1 = new Dog("Bantay","Corgi",3);
// console.log(dog1);

// let person1 = {
// 	name:"Saitama",
// 	heroName:"One Punch Man",
// 	age: 30
// };

// console.log(person1);

// Mutator Method
// student.push("Henry");
// console.log(students);

// student.unshift("Jeremiah");
// console.log(students);

// student.unshift("Christine");
// console.log(students);

// student.pop("");
// console.log(students);

// student.shift();
// console.log(students);

// Non-Mutator Method

// Iterator Method
// every() - loops and check if all items satisfy a given condiion and returns a boolean.

// ex.
// let arrNum = [15,25,50,20,10,11];
// let allDivisible;
// check if every item in arrNum is divisible by 5:
/*arrNum.forEach(num=>{
	if(num%5 === 0){
		console.log(`${num} is divisible by 5.`)
	} else{
		allDivisible = false;
	}
	// However, can forEach() return data that will tell us IF all numbers/items in our arrNum array is divisible by 5?
})*/
// console.log(allDivisible);

// every()
// ex.
// arrNum.pop();
// arrNum.push(35);
// let divisibleBy5 = arrNum.every(num => {
	// ever() is able to return data.
	// When the function is inside every() is able to return true, the loop will continue until all items are able to return true. Else, IF at least one item returns false, then the loop will stop there and every() will return false.
// 	console.log(num);
// 	return num%5 === 0;
// })

// console.log(divisibleBy5);

// some() - loops and checks if at least one item satisfies a given condition and returns a boolean. It will true if at least ONE item satisfies the condition, else IF no item returns true/satisfies the condition, some() will return false.

// let someDivisibleBy5 = arrNum.some(num => {
	// some() will stop its loop once a function/item is able to return true.
	// Then, some() will return true else, if all items does not satisfy the condition or return true, some() will return false.
	// console.log(num);
	// return num % 5 === 0;
// });
// console.log(someDivisibleBy5);

// let someDivisibleBy11 = arrNum.some(num => {
// 	console.log(num);
// 	return num%6 === 0;
// })
// console.log(someDivisibleBy11);


// QUIZ
/*
	1. Using Array Literals, []
	2. using Array Indexes, arrName[0]
	3. Using Array Length, arrName[arrName.length-1] 
	4. find()
	5. forEach()
	6. map()
	7. every()
	8. some()
	9. TRUE
	10. TRUE
	11. FALSE
	12. Math
	13. TRUE
	14. FALSE
	15. TRUE
*/

// FUNCTION CODING
let addToEnd = (arrName,arrItem) =>{
	if(typeof arrItem === "string"){
		arrName.push(arrItem);
		return arrName
	} else {
		return "error - can only add strings to an array";
	}
}

let addToStart = (arrName, arrItem) => {
	if(typeof arrItem === "string") {
		arrName.unshift(arrItem);
		return arrName
	} else {
		return "error - can only add strings to an array";	
	}
}

let elementChecker = (arrName, arrItem) => {
	if(arrName.length === 0) {
		return "error - passed in array is empty"
	} else{
		return arrName.includes("Jane")
	}
}

let stringLengthSorter = (arr) => {
	let checkArrItem = arr.every(arrItem =>{
		return typeof (arrItem) === "string"
	})
	if(checkArrItem === false) {
		return "all array elements must be string"
	} else {
		arrLenght = arr.sort((itemOne,itemTwo) =>{
			itemOne.length - itemTwo.length
		})
		return arrLenght
	}
}